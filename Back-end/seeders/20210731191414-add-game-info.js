'use strict';

module.exports = {
  up: async (queryInterface, Sequelize) => {
      await queryInterface.bulkInsert('games', [{
        id:1,
        name: 'rockpaperscissor',
        description: "Traditional Rock Paper Scissor Game",
        thumbnail_url: '/thumbnail/rockpaperscissorthumbnail.png',
        game_url:'/game/rockpaperscissor.png',
        play_count:0,
        createdAt: new Date(),
        updatedAt: new Date()
      }], {});
    
  },

  down: async (queryInterface, Sequelize) => {
    await queryInterface.bulkDelete('games', null, {});
  }
};
