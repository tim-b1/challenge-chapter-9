const express = require('express');
const router = express.Router();
const profileController = require("../controllers/profileController");
const restrict = require('../middleware/restrict')
const blackListCheck = require('../middleware/blackListToken');

/* HANDLING USER PROFILE. */
router.put('/edit', restrict, profileController.edit);
router.delete('/delete', restrict, profileController.delete);
router.get('/get', restrict, blackListCheck, profileController.get);
router.get('/getall', restrict, blackListCheck, profileController.getall);

module.exports = router;