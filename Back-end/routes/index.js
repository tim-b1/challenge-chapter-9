const express = require('express');
const router = express.Router();
const homeRouter = require("./home");
const gameRouter = require("./game");
const profileRouter = require("./profile");

router.use("/", homeRouter);
router.use("/game", gameRouter);
router.use("/profile", profileRouter);

module.exports = router;
