const express = require('express');
const router = express.Router();
const gameController = require("../controllers/gameController");
const restrict = require('../middleware/restrict');
const blackListCheck = require('../middleware/blackListToken');

/* GET users listing. */
router.put('/increasePlayCount/:name', restrict, blackListCheck, gameController.increasePlayCount)
router.get('/thumbnails',restrict, blackListCheck, gameController.getAllThumbnails);
router.get('/:name', restrict, blackListCheck, gameController.getDetailByName);

module.exports = router;