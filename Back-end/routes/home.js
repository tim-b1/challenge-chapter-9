const express = require('express');
const router = express.Router();
const homeController = require("../controllers/homeController");
const restrict = require('../middleware/restrict')
const logout = require('../middleware/logOutToken');
const blackListCheck = require('../middleware/blackListToken');

/* GET users listing. */
router.post('/login', homeController.login);
router.post('/register', homeController.register);
router.get('/logout', restrict, blackListCheck,logout, homeController.logout);
router.post('/forgot-password', homeController.forgotPassword)
module.exports = router;