const path = require('path');
const multer = require('multer');

const storageImage =multer.diskStorage({
    destination: (req, file, cb) => {
        cb(null, path.join(__dirname, '../public/game'))
    },
    filename: (req, file, cb) => {
        cb(null, new Date().toISOString() + file.originalname)
    }
}) 

const storageThumbnail =multer.diskStorage({
    destination: (req, file, cb) => {
        cb(null, path.join(__dirname, '../public/thumbnail'))
    },
    filename: (req, file, cb) => {
        cb(null, new Date().toISOString() + file.originalname)
    }
})

const uploadGame= multer({storage: storageImage} )
const uploadThumbnail = multer({storage : storageThumbnail})

module.exports = {uploadGame, uploadThumbnail}

