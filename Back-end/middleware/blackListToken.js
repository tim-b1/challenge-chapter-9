module.exports = (req, res, next) => {
    const { username, token, redisClient } = req;
    // Check redis if the username exists
    redisClient.get(username, (err, data) => {
        if (err) {
          return res.status(400).json({
            code: 150,
            message: err.message,
        });
        }
        // if username exists, check whether token is blacklisted or not
        if (data !== null) {
          const userData = JSON.parse(data);
          if (userData[username].includes(token)) {
            return res.status(400).json({
                code: 150,
                message: "Please Log in"
            });
          }
          return next();
        }
        return next()
    });
};