module.exports = (req,res, next) => {
    const {username, token, redisClient} = req;
    // Using redist to blacklist token 
    //finding if user exist in blacklist item
    redisClient.get(username, (err, data) => {
        if(err) {
            res.status(400).json({
                code: 200,
                message: err.message
            });
        }
        // if username existed
        if(data !== null) {
            const parsedData = JSON.parse(data);
            parsedData[username].push(token);
            redisClient.setex(username, 3600, JSON.stringify(parsedData));
            return next();
        }
        //if username not existed
        const newBlackList = { [username] :[token] };
        redisClient.setex(username, 3600, JSON.stringify(newBlackList));
        return next();
    })
}