const nodemailer = require('nodemailer');
const {google} = require('googleapis');
const config = require('./emailConfig')

const oauthClient = new google.auth.OAuth2(config.clientId, config.clientSecret);
oauthClient.setCredentials( { refresh_token : config.refresh_token });

const sendMail = (recipient, newPassword) => {
    const accessToken = oauthClient.getAccessToken();
    const transport = nodemailer.createTransport({
        service: 'gmail', 
        auth: {
            type: 'OAUTH2',
            user: config.user,
            accessToken: accessToken,
            clientId: config.clientId,
            clientSecret: config.clientSecret,
            refreshToken: config.refresh_token,            
        }
    })

    const mailOptions = {
        from: `Rock Paper Scissor <${config.user}>`,
        to: recipient,
        subject: 'Password Reset Request',
        text: textMessage(recipient, newPassword),
    }

    transport.sendMail(mailOptions, function(err, info) {
        if(err) {
            throw new(err);
        }
        transport.close();
    })
}

const textMessage = (recipient, newPassword) => {
    return `
    Hello, ${recipient}! 
    This is your new password ${newPassword} `
    
}

module.exports = {sendMail};
