require('dotenv').config();
const redisConfig = require('./redisConfig')
const jwt = require('jsonwebtoken');
const redis = require("redis");
const redisClient = redis.createClient(redisConfig.port, redisConfig.host);
redisClient.auth(redisConfig.pass);

module.exports = (req, res, next) => {
    try {
        const header= req.header('Authorization');
        if(!header) {
            return res.status(401).json({
                code: 150,
                message: "no token."
            });
        }
        const [type, token] = header.split(' ');
        if(typeof token !== 'undefined' && type === 'Bearer') {
            jwt.verify(token, process.env.jwt_secret, (err,decode) => {
                if(err) {
                    return res.status(400).json({
                        code: 150,
                        message: err.message
                    });
                } else {
                    // Append parameter to req object
                    req.username = decode.username;
                    req.token = token;
                    req.redisClient = redisClient;
                    return next();
                }
            });
        } else {
            return res.status(401).json({
                code: 150,
                message: "invalid token."
            });
        }
    } catch(err) {
        return res.status(500).json({
            code: 150,
            message: message.err
        });
    }
} 