'use strict';
const { Model } = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class game extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
    }
  };
  game.init({
    name: {type: DataTypes.STRING, allowNull: false, unique: true},
    description: DataTypes.STRING,
    thumbnail_url: DataTypes.STRING,
    game_url: {type : DataTypes.STRING, allowNull: false},
    play_count: DataTypes.INTEGER,
  }, {
    sequelize,
    modelName: 'game',
  });
  return game;
};