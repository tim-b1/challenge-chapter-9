'use strict';
const { Model } = require('sequelize');
const bcrypt = require('bcryptjs');
const jwt = require('jsonwebtoken');
require('dotenv').config();

module.exports = (sequelize, DataTypes) => {
  class user extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
    }

    static encrypt = (password) => bcrypt.hashSync(password, 0);

    static register = async (body) => {
      const existingEmail = await this.findOne({where : {email : body.email}});
      const existingUsername = await this.findOne({where : {username : body.username}})
      const checkFormat = body.email.split("@");
      if (existingEmail) {
        throw new Error('Account already exists');
      } else if (existingUsername) {
        throw new Error('Cant use username');
      } else if (!checkFormat) {
        throw new Error('not email type')
      } else {
        const encryptedPassword = this.encrypt(body.password); 
        return this.create({ ...body, password : encryptedPassword })
      }
    }

    checkPassword = async (password) => await bcrypt.compare(password, this.password);

    generateToken = () => {
      const payload = { username: this.username, email: this.email};
      const token = jwt.sign(payload, process.env.jwt_secret, {expiresIn: '10h'});
      return token;
    }

    static authenticate = async ({ email, password }) => {
      const user = await this.findOne({ where : { email:email }});
      if (!user) {
        throw new Error ('invalid email or password');
      }
      const isPasswordValid = await user.checkPassword(password);
      if (!isPasswordValid) {
        throw new Error ('invalid email or password');
      }
      return Promise.resolve (user);
    } 
  };
  user.init({
    email: { type : DataTypes.STRING, allowNull : false},
    username: { type : DataTypes.STRING, allowNull : false},
    password: { type : DataTypes.TEXT, allowNull : false},
    total_score: DataTypes.INTEGER,
    bio: DataTypes.STRING,
    city: DataTypes.STRING,
    social_media_url: DataTypes.STRING
  }, {
    sequelize,
    modelName: 'user',
  });
  return user;
};