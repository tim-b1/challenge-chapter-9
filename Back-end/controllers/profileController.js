const {user} = require('../models')
const jwt = require('jsonwebtoken');
const { Op } = require("sequelize");
const logout = require('../middleware/logOutToken');
const { gmail } = require('googleapis/build/src/apis/gmail');

const formatScore = async (email) => {
    const found = await user.findOne({where : {email : email }})
    return {
        total_score : found.total_score,
    }
}

const format = async (email) => {
    const found = await user.findOne({where : {email : email }})
    return {
        token : found.generateToken(),
    }
}
exports.edit = async (req, res) => {
    try {
        const header= req.header('Authorization');
        const [type, token] = header.split(' ');
        const payload = jwt.verify(token, process.env.jwt_secret);
        const oldinfo = await user.findOne({where : {email : payload.email}})
        if(!oldinfo) {
            return res.status(400).json({
                code: 100,
                message: "Token invalid",
            });
        }
        if(!!req.body.password) {
            req.body.password = user.encrypt(req.body.password);
        }
        if('total_score' in req.body) {
            if(oldinfo.total_score !== null) {
                req.body.total_score = oldinfo.total_score+ parseInt(req.body.total_score)
            }
        }
        if(('email' in req.body) && oldinfo.email !== req.body.email) {
            
            const sameEmail = await user.findOne({where : {email : req.body.email}})
            if(sameEmail) {
                return res.status(400).json({
                    code: 100,
                    message: "Account exist",
                });
            }
        }
        if(!!req.body.username && oldinfo.username !== req.body.username) {
            const sameUsername = await user.findOne({where : {username : req.body.username}})
            if(sameUsername) {
                return res.status(400).json({
                    code: 100,
                    message: "Account exist",
                });
            }
        }
        if(!!req.body.email || !!req.body.username) {
            if(!oldinfo) {
                await logout(req,res,() => { return });
                return res.status(400).json({
                    code: 100,
                    message: "please log in, token invalid",
                });
            }
            
            if(((oldinfo.email !== req.body.email) && req.body.email) || (req.body.username && (oldinfo.username !== req.body.username))) {
                await user.update(req.body, {where : {email : payload.email}});
                await logout(req,res,() => { return });
                return res.status(200).json({
                    code: 0,
                    message: "Edit Success",
                    data: await format(!!req.body.email ? req.body.email : oldinfo.email),
                }); 
            }
        }
        const [found] = await user.update(req.body, {where : {email : payload.email}});
        if(!found) {
            return res.status(400).json({
                code: 100,
                message: "Account doesnt exist",
            });
        }
        if(!!req.body.total_score) {
            return res.status(200).json({
                code: 0,
                message: "Edit Success",
                data: await formatScore(payload.email),
            }); 
        }
        res.status(200).json({
            code: 0,
            message: "Edit Success",
        }); 
    } catch (err) {
        console.error(err);
        res.status(500).json({
            code: 200, 
            message: err.message
        })
    }
}

exports.delete = async (req, res) => {
    try {
        const header= req.header('Authorization');
        const [type, token] = header.split(' ');
        const payload = jwt.verify(token, process.env.jwt_secret);
        const all = await user.findAll();
        // Creating dummy data so that the table not empty
        if(all.length === 1) {
            const randomString = Math.random().toString(36).substring(2,8);
            await user.create({
                email:`${randomString}@dummy.com`,
                password: user.encrypt(randomString),
                username: randomString,
            });
        }
        const found = await user.destroy({where : {email:payload.email}})
        if(!found) {
            return res.status(400).json({
                code: 100,
                message: "Account doesnt exist",
            });
        }
        await logout(req,res,() => { return });
        res.status(200).json({
            code: 0,
            message: "Delete Success",
        }); 
    } catch(err) {
        res.status(500).json({
            code: 200, 
            message: err.message
        })
    }
}

exports.get = async (req, res) => { 
    try {
        const header= req.header('Authorization');
        const [type, token] = header.split(' ');
        const payload = jwt.verify(token, process.env.jwt_secret);
        const found = await user.findOne({
            where: { email : payload.email},
            attributes: ['email', 'username', 'total_score', 'bio', 'city', 'social_media_url']
        });
        if(!found) {
            return res.status(400).json({
                code: 100,
                message: "Account doesnt exist",
            });
        } 
        res.status(200).json({
            code: 0,
            message: "OK",
            data: found,
        })
    } catch (err) {
        res.status(500).json({
            code: 200, 
            message: err.message
        })
    }
}

exports.getall = async (req,res) => {
    try {
        const check = await user.findAll();
        if(!check) {
            return res.status(400).json({
                code: 100,
                message: "No Account registered",
            });
        }
        const found = await user.findAll(
            { where : 
                {total_score: {[Op.not]: null}},
                order: [["total_score", "DESC"]], 
                attributes: ['email','username', 'total_score', 'bio', 'city', 'social_media_url']
            }
        )
        if(!found.length || found[0].total_score < 1) {
            return res.status(400).json({
                code: 100,
                message: "No user got score",
            });
        }
        return res.status(200).json({
            code: 0,
            message: "OK",
            data: found
        })
    } catch (err) {
        return res.status(500).json({
            code: 200, 
            message: err.message
        })
    }
}

