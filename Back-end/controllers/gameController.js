const {game} = require('../models')

exports.increasePlayCount = async (req,res) => {
    try {
        const gameUpdate = await game.findOne({where : {name : req.params.name}});
        if(!gameUpdate) {
            res.status(400).json({
                code: 100,
                message: "No game",
            });
            return;
        }
        const newPlayCount = gameUpdate.play_count + req.body.number;
        await game.update({play_count:newPlayCount}, {where : {name : req.params.name}});
        res.status(200).json({
            code: 0,
            message: "OK",
        })
    } catch(err) {
        console.error(err)
        res.status(500).json({
            code: 200,
            message: err.message
        });
    }
}

exports.getDetailByName = async (req,res) => {
    try {
        const theGame = await game.findOne({
            where : {name : req.params.name},
            attributes: ['name', 'description','thumbnail_url','game_url','play_count']
        });
        if(!theGame) {
            res.status(400).json({
                code: 100,
                message: "No game",
            });
            return;
        }
        res.status(200).json({
            code: 0,
            message: "OK",
            data: theGame
        })
    } catch (err) {
        res.status(500).json({
            code: 200,
            message: err.message
        });
    }
}

exports.getAllThumbnails = async (req,res) => {
    try {
        const theGame = await game.findAll({attributes : ['name','thumbnail_url']});
        if(!theGame) {
            res.status(400).json({
                code: 100,
                message: "No game",
            });
            return;
        }
        res.status(200).json({
            code: 0,
            message: "OK",
            data: theGame
        })
    } catch (err) {
        res.status(500).json({
            code: 200,
            message: err.message
        });
    }
}