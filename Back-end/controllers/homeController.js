const {user} = require('../models')
const {sendMail} = require('../middleware/nodemailer');

function format (user_game) {
    return {
        token : user_game.generateToken(),
    }
}

exports.register = async (req, res) => {
    if (!req.body.username || !req.body.email || !req.body.password) {
        res.status(400).json({
          code: 100,
          message: "username or email or password field cannot be empty."
        });
        return;
    }
    try {
        const found = await user.register(req.body);
        if(!found) {
            res.status(500).json({
                code: 100,
                message: "Server error creating"
            });
            return;
        }
        res.status(201).json({
            code: 0,
            message: "Player created",
        });    
    } catch (err) {
        if(err.message == 'Account already Exist' || err.message == 'Cant use username') {
            res.status(400).json({
                code: 100,
                message: err.message,
            });
            return;
        }
        res.status(500).json({
            code: 200,
            message: err.message
        });
    }
}

exports.login = async (req, res) => {
    if (!req.body.email || !req.body.password) {
        res.status(400).json({
          code: 100,
          message: "username or email or password field cannot be empty."
        });
        return;
    }
    try {
        const found = await user.authenticate(req.body);
        res.status(200).json({
            code: 0,
            message: "OK",
            data: format(found),
        }); 
    } catch (err) {
        if(err.message == 'invalid email or password') {
            res.status(400).json({
                code: 100,
                message: 'invalid email or password',
            });
        } else {
            res.status(500).json({
                code: 200,
                message: err.message
            });
        }
    }
}

exports.logout = async (req, res) => {
    return res.status(200).json({
        code: 0,
        message: "Logout Successfull",
    }); 
}

exports.forgotPassword = async (req, res) => {
    try{
        const newPassword = Math.random().toString(36).substring(2,8);
        const encryptedPassword = user.encrypt(newPassword);
        const [found] = await user.update({password : encryptedPassword}, {where : {email : req.body.email}});
        if(!found) {
            return res.status(400).json({
                code:100, 
                message: "Account doesn't exist"
            })
        }
        sendMail(req.body.email, newPassword);
        return res.status(200).json({
            code:0,
            message: "Please check your email inbox"
        })
    } catch (err){
        return res.status(500).json({
            code:200,
            message: err.message,
        })
    }
}