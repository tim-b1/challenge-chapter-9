import React from "react";
import "./App.css";
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom';
import Layout from './component/Layout';
import Game from './pages/game'
import Login from './pages/login'
import Register from './pages/register'
import ForgotPassword from "./pages/forgot-password";
import { UserProvider } from "./component/userContext";
import ProtectedRoute from "./component/protectedRoute";
import EditProfile from "./pages/edit-profile";
import GameList from "./pages/game-list";
import DetailPage from "./pages/detail-page";
import Profile from "./pages/profile";
import Home from "./pages/home";
import LandingPage from "./pages/landing-page";
function App() {
  return (
      <Router>
        <UserProvider>
          <Layout>
            <Switch>
              <Route exact path="/" component={LandingPage} />
              <Route exact path="/login" component={Login}/>
              <Route exact path="/register" component={Register}/>
              <Route exact path="/forgot-password" component={ForgotPassword}/>
              <ProtectedRoute exact path="/home" component={Home}/>
              <ProtectedRoute exact path="/profile" component={Profile}/>
              <ProtectedRoute exact path="/edit-profile" component={EditProfile}/>
              <ProtectedRoute exact path="/game/:name" component={Game}/>
              <ProtectedRoute exact path="/game-list" component={GameList}/>
              <ProtectedRoute exact path="/game-list/:name" component={DetailPage}/>
            </Switch>
            </Layout>

        </UserProvider>
      </Router>
  );
}

export default App;
