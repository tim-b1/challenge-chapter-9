import { useEffect, useState } from "react"
import { useHistory } from "react-router";
import { Link } from "react-router-dom";
import { useUser } from "../component/userContext"

const Profile = () => {
    const {request, setActiveAccount, globalError,activeAccount, setActiveUser, setGlobalError} = useUser();
    const [loading, setLoading] = useState(false);
    const [error, setError] = useState("");
    let history = useHistory();

    function handleLogOut(e) {
        e.preventDefault();
        setLoading((oldLoading) => {return true;});
        setError((olError) => {return null})
        try {
            request('/api/logout', 'GET').then((data) => {
                if(data.code > 0) {
                    if(data.code === 150) {
                        setGlobalError((oldError) => {return data.message})
                    }
                    setError( (oldError) => {return data.message });
                  } else {
                    setActiveUser((old) => {return null})
                    localStorage.removeItem('token');
                    localStorage.removeItem('account');
                    localStorage.removeItem('user');
                  }
            }).catch((err) =>{
                setError((oldErr) => {return err.message})
                })
        } catch(err) {
            setError((olError) => {return err.message})
        }
    }

    useEffect (() => {
        try {
            setError((oldErr) => {return null})
            setGlobalError((oldErr) => {return null})
            setLoading((oldLoading) => {return true});
            request('/api/profile/get','GET').then((data) => {
                if(data.code > 0) {
                    setActiveAccount((oldAccount) => {return null});
                    setActiveUser((oldUser) => {return null});
                    setGlobalError((oldError) => {return data.message})
                } else {
                    setActiveAccount((oldAccount) => {return data.data});
                    setActiveUser((old) => {return data.data.email})
                }
            }).catch((err) => {
                setError((oldErr) => {return err.message})
              })
        } catch (err) {
            setError((oldError) => {return err.message})
        }
    },[])

    useEffect( () => {
        console.log(activeAccount)
        if(activeAccount) {
            setLoading((oldLoading) => {return false});
        }
        if(!!globalError) {
            history.push('/login')
        } return () => {
            setLoading((oldLoading) => {return false});
        }
    },[globalError,activeAccount])
    
    //     return content;
    return (
        <div className="p-0">
            { error && <div className="alert alert-danger">{error}</div>}
            {!loading && activeAccount && <div className="alert alert-success">Wellcome {activeAccount.username}</div>}
            {!loading && activeAccount &&
                <div className="card d-flex align-content-center">
                    <div className="card-header">
                        {activeAccount.email}
                        <Link to="/edit-profile" className="text-center mx-3 btn btn-outline-success">Edit Profile</Link>
                        <span onClick={handleLogOut} className="text-center mx-1 btn btn-outline-danger">Logout</span>
                    </div>
                    <ul className="list-group list-group-flush p-3">
                        <div className="input-group mb-3 mt-3">
                            <div className="input-group-prepend">
                                <span className="input-group-text">User Name</span>
                            </div>
                            <input style={{border:"0",backgroundColor:"transparent"}} className="form-control" disabled={true} value={activeAccount.username}/>
                        </div>
                        <div className="input-group mb-3">
                            <div className="input-group-prepend">
                                <span className="input-group-text">Total Score</span>
                            </div>
                            <input style={{border:"0", backgroundColor:"transparent"}} className="form-control" disabled={true} value={activeAccount.total_score}/>
                        </div>
                        <div className="input-group mb-3">
                            <div className="input-group-prepend">
                                <span className="input-group-text">Bio</span>
                            </div>
                            <input style={{border:"0", backgroundColor:"transparent"}} className="form-control" disabled={true} value={activeAccount.bio}/>
                        </div>
                        <div className="input-group mb-3">
                            <div className="input-group-prepend">
                                <span className="input-group-text">City</span>
                            </div>
                            <input style={{border:"0", backgroundColor:"transparent"}} className="form-control" disabled={true} value={activeAccount.city}/>
                        </div>
                        <div className="input-group mb-3">
                            <div className="input-group-prepend">
                                <span className="input-group-text">social media</span>
                            </div>
                            <input style={{border:"0", backgroundColor:"transparent"}} className="form-control" disabled={true} value={activeAccount.social_media_url}/>
                        </div>
                    </ul>
                    
                </div>
                }
        </div>
    )
}

export default Profile