import React, { useEffect, useRef, useState } from "react";
import { useHistory } from "react-router-dom";
import  {useUser}  from "../component/userContext";
import Style from "../style/index.module.css";

const EditProfile = () => {
    let history = useHistory();
    const {activeAccount, setActiveUser, request, setActiveAccount, globalError, setGlobalError} = useUser();
    const [error, setError] = useState(null);
    const [loading, setLoading] = useState(false);
    const emailRef = useRef();
    const passwordRef = useRef();
    const usernameRef = useRef();
    const bioRef = useRef();
    const cityRef = useRef();
    const socialRef = useRef();

    function handleSubmit (e) {
        e.preventDefault();
        setError((oldErr) => { return null})
        setGlobalError((oldErr) => { return null})
        const account = {
            email : emailRef.current.value,
            password:  passwordRef.current.value,
            username: usernameRef.current.value,
            bio: bioRef.current.value,
            city: cityRef.current.value,
            social_media_url: socialRef.current.value,
        }
        let object = new Object();
        for(let property in account) {
            if(account[property] !== "") {
                Object.assign(object, {
                    [property] : account[property]
                })
            }
        }
        if(activeAccount) {
            let a = true;
            for (let property in object) {
                if(object[property] === activeAccount[property] ) {
                    a = true && !!a;
                } else {
                    a = false && !!a;
                }
            }
            console.log(a)
            if(a === true && !object.password) {
                setError((oldErr) => {return "Data Edited"})
                return;
            }
        }        
        try {
            setLoading((oldLoading) => { return true})
            request('/api/profile/edit','PUT', object).then( (data) => {
                if(data.code > 0) {
                    if(data.code === 150) {
                        setGlobalError((oldError) => {return data.message})
                    }
                    setError( (oldError) => {return data.message });
                } else {
                    if('data' in data && 'token' in data.data) {
                        localStorage.setItem('token', data.data.token);
                    }
                    setActiveUser((oldActiveUser) => {return object.email ? object.email : oldActiveUser});
                    setActiveAccount((oldAccount) => { 
                        for(let property in object) {
                            oldAccount[property] = object[property]
                        }
                        return {...oldAccount}
                    })
                    setError((oldErr) => {return data.message})
                }
                }).catch((err) => {
                    setError((oldError) => {return err.message})
                })
        } catch(err) {
            setError( (oldError) => { return err.message })
        }
    }

    useEffect( () => {
        if(activeAccount || error) {
            setLoading((oldLoading) => { return false})
        }
        if(!!globalError) {
            history.push('/login')
        } return () => {
            setLoading((oldLoading) => { return null});
        }
    },[globalError,activeAccount, error])

    function handleDelete(e) {
        if(window.confirm('This action cannot be undone. Are you sure you want to delete your account?')) {
            e.preventDefault();
            try {
                setLoading((oldLoading) => { return true})
                request('/api/profile/delete', 'DELETE').then( (data) => {
                    if(data.code > 0) {
                        if(data.code === 150) {
                            setGlobalError((oldError) => {return data.message})
                        }
                        setError((oldError) => { return data.message});
                    } else {
                        localStorage.removeItem("token");
                        localStorage.removeItem("user");
                        localStorage.removeItem("account");
                        setActiveUser((oldUser) => {return null});
                        setActiveAccount((oldAccount) => {return null});
                    }
                }).catch((err) => {
                    setError((oldError) => {return err.message})
                })
            } catch(err) {
                setError( (oldErr) => {return err.message});
            }
        }
    }
    return (
        <div className={Style.card}>
            <h2>Edit Profile</h2>
            { error && <div className="alert alert-warning">{error}</div>}
            <form onSubmit={handleSubmit}>
                <div className={Style.inputBorder}>
                    <input ref={usernameRef} type="text" id="name" className={Style.text} name="name" required/>
                    <label htmlFor="name">Name</label>
                    <div className={Style.border}></div>
                </div>
                
                <div className={Style.inputBorder}>
                <input ref={emailRef} type="email" id="email" className={Style.text} name="email"/>
                <label htmlFor="email">Email</label>
                <div className={Style.border}></div>
                </div>
                
                <div className={Style.inputBorder}>
                <input ref={passwordRef} type="password" id="pasword" className={Style.text} name="password"/>
                <label htmlFor="password">Password</label>
                <div className={Style.border}></div>
                </div>
                
                <div className={Style.inputBorder}>
                <input ref={bioRef} type="text" id="bio" className={Style.text} name="bio"/>
                <label htmlFor="bio">Bio</label>
                <div className={Style.border}></div>
                </div>

                <div className={Style.inputBorder}>
                <input ref={cityRef} type="text" id="city" className={Style.text} name="city"/>
                <label htmlFor="city">City</label>
                <div className={Style.border}></div>
                </div>

                <div className={Style.inputBorder}>
                <input ref={socialRef} type="text" id="social_media_url" className={Style.text} name="social_media_url"/>
                <label htmlFor="social_media_url">Social Media</label>
                <div className={Style.border}></div>
                </div>

                <button disabled={loading} type="submit" className={Style.btn} value="Log In">Edit</button>
            </form>
            <div className="d-flex">
                <button disabled={loading} onClick={handleDelete} type="button" className="opacity-75 text-white mt-3 btn btn-outline-danger mx-1">Delete Account</button>
            </div>
        </div>
    );
    };

export default EditProfile;
