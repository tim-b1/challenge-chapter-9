import React, { useRef, useState } from "react";
import Style from "../style/index.module.css";
import { Link } from "react-router-dom";
import { useUser } from "../component/userContext";

const ForgotPassword = () => {
  const {request, setActiveAccount, setActiveUser} =useUser();
  const [message, setMessage] = useState("")
  const [loading, setLoading] = useState(false)
  const emailRef = useRef();

  function handleForgotPassword (e) {
    e.preventDefault();
    try{
      console.log(emailRef.current.value)
      setLoading((oldLoading) => {return true})
      request('/api/forgot-password', 'POST', {email :emailRef.current.value}).then( (data) => {
        setMessage((oldMessage) => { return data.message})
        setActiveUser((oldUser) => {return null});
        setActiveAccount((oldAccount) => {return {}});
        if(localStorage.getItem('token')) {
          localStorage.removeItem('token');
        }
      })
    } catch(err) {
      setMessage((oldMessage) => { return err.message})
    }
    
  }

  return (
    <div className={Style.card}>
      <h2>Forgot Password</h2>
      <form onSubmit={handleForgotPassword}>
      { message && <div className="alert alert-warning">{message}</div>}
        <div className={Style.inputBorder}>
          <input ref={emailRef} type="email" className={Style.text} name="email" required/>
          <label>Enter email address</label>
          <div className={Style.border}></div>
        </div>
        
        <button disabled={loading} type="submit" className={Style.btn} value="Log In">Reset  Password</button>
      </form>
      <Link className="text-white text-decoration-none opacity-75" to="/login"><br/>Login</Link>
    </div>
  );
};

export default ForgotPassword;
