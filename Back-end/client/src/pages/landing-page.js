import { useEffect } from "react";
import { Link } from "react-router-dom";
import { useUser } from "../component/userContext";
import Background from '../style/backgound-image/main-bg.jpg';
import Style from "../style/index.module.css";


const LandingPage = () => {
    const{setGlobalError} = useUser();
    useEffect(() => {
        if(!localStorage.getItem('token'))
        setGlobalError((oldError) => {return null})
    },[])
    return(
        <div style={{borderRadius:"6px", backgroundImage: `url(${Background})`,backgroundSize:"cover", height:"700px", position:"relative"}} className="d-flex justify-content-center">
            <div style={{position: "absolute", top:"40%"}}  className= "container-fluid">   
                <div className= "text-center text-white">         
                    <h1 className={Style.playTraditionalGame} > PLAY TRADITIONAL GAME</h1>
                </div>

                <div className= "text-center text-white">         
                    <p className={Style.subPlayGame}> Experience new traditional game play</p>
                </div>
                <div className= "text-center">
                <Link to={localStorage.getItem('token') ? "/home" : "/login"} type="button" className="btn btn-warning btn-lg">Log In</Link>
            </div>
            </div>
        </div>
    )
}

export default LandingPage;
