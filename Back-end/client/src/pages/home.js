import { useEffect, useState } from "react"
import { useHistory } from "react-router";
import { useUser } from "../component/userContext"
import Style from '../style/index.module.css'
import { Collapse, CardBody, Card } from 'reactstrap';

const Home = () => {
    const {request, setActiveAccount, globalError,activeAccount, setActiveUser, setGlobalError} = useUser();
    const [loading, setLoading] = useState(false);
    const [error, setError] = useState("");
    let history = useHistory();
    const [isOpen, setIsOpen] = useState(false);
    const [isOpenTwo, setIsOpenTwo] = useState(false);
    const [isOpenThree, setIsOpenThree] = useState(false);

    const toggle = () => setIsOpen(!isOpen);
    const toggleTwo = () => setIsOpenTwo(!isOpenTwo);
    const toggleThree = () => setIsOpenThree(!isOpenThree);

    useEffect (() => {
        try {
            setError((oldErr) => {return null})
            setGlobalError((oldErr) => {return null})
            setLoading((oldLoading) => {return true});
            request('/api/profile/get','GET').then((data) => {
                if(data.code > 0) {
                    setActiveAccount((oldAccount) => {return null});
                    setActiveUser((oldUser) => {return null});
                    setGlobalError((oldError) => {return data.message})
                } else {
                    setActiveAccount((oldAccount) => {return data.data});
                    setActiveUser((old) => {return data.data.email})
                }
            }).catch((err) => {
                setError((oldErr) => {return err.message})
              })
        } catch (err) {
            setError((oldError) => {return err.message})
        }
    },[])

    useEffect( () => {
        if(activeAccount) {
            setLoading((oldLoading) => {return false});
        }
        if(!!globalError) {
            history.push('/login')
        } return () => {
            setLoading((oldLoading) => {return false});
        }
    },[globalError,activeAccount])

    return (
        // System Requirements
        <>
        {!loading &&
        <div className="p-0">
            { error && <div className="alert alert-danger">{error}</div>}
            <div className={"p-3 " + Style.systemReq} style={{borderRadius:"6px"}}>
                <div className="container-fluid p-3">
                    <div className="row p-3">

                        <h1 className="col-12 col-xl-4 text-white opacity-75">SYSTEM REQUIREMENTS</h1>
                        <p className="pb-3 text-white opacity-50 col-12 col-xl-8 justify-content-lg-start">Can my Computer Run this game?</p>
                        
                        <div style={{overflowX:"auto"}} className="col-12 col-xl-6 pt-xl-5 mt-xl-2">
                            <table className="table table-bordered text-white opacity-75">
                                <tr className="m-0"> 
                                    <td>
                                        <div className="text-warning text font-weight-bold">OS:</div>
                                        <div>Windows 7 64-bit only</div>
                                        <div>(No OSX support at this time)</div>
                                    </td>
                                    <td>
                                        <div className="text-warning text font-weight-bold">PROCESSOR:</div>
                                        <div>Intel COre 2 Duo @ 2.4 GHZ or</div>
                                        <div>AMD Athlon X2 @ 2.8 GHZ</div>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <div className="text-warning text font-weight-bold">MEMORY:</div>
                                        <div>4 GB RAM</div>
                                    </td>
                                    <td>
                                        <div className="text-warning text font-weight-bold">Storage:</div>
                                        <div>8 GB availabe space</div>
                                    </td>
                                </tr>
                                <tr>
                                    <td colSpan="2">
                                        <div className="text-warning font-weight-bold">GRAPHICS:</div>
                                        <div>NVIDIA Geforce GTX 660 2GB or</div>
                                        <div> AMD Radeon HD 7850 2GB DirectX11 (Shader Model 5)</div>
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </div>
                </div>
            </div>

            <div className={Style.gameFeature + " mt-3 p-3"} style={{borderRadius:"6px"}}>
                <div className="col-sm-9 offset-sm-3 col-md-6 offset-md-6 p-3">
                    <div className= "p-3 text-white opacity-75">
                        <h5>What's so special?</h5>
                        <h1 className="font-weight-bold">FEATURES</h1>
                    </div>
                    <div className= "p-3">
                        <div type="button" className="mb-3 pb-3 text-warning opacity-75" onClick={toggle}>TRADITIONAL GAMES</div>
                        <Collapse isOpen={isOpen}>
                        <Card style={{backgroundColor:"transparent", border:"0"}}>
                            <CardBody className="text-white">
                            If you miss your childhood, we provide many traditional game here
                            </CardBody>
                        </Card>
                        </Collapse>

                        <div type="button" className="mb-3 pb-3 text-warning opacity-75" onClick={toggleTwo}>Game Suit</div>
                        <Collapse isOpen={isOpenTwo}>
                        <Card style={{backgroundColor:"transparent", border:"0"}}>
                            <CardBody className="text-white">
                            If you miss your childhood, we provide many traditional game here
                            </CardBody>
                        </Card>
                        </Collapse>

                        <div type="button" className="text-warning opacity-75" onClick={toggleThree}>TBD</div>
                        <Collapse isOpen={isOpenThree}>
                        <Card style={{backgroundColor:"transparent", border:"0"}}>
                            <CardBody className="text-white">
                            If you miss your childhood, we provide many traditional game here
                            </CardBody>
                        </Card>
                        </Collapse>
                    </div>  
                </div>
            </div>
        </div>
    }
    </>
    )
}

export default Home;