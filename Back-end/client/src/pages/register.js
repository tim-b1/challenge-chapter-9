import React, { useEffect, useRef, useState } from "react";
import { Link, useHistory } from "react-router-dom";
import  {useUser}  from "../component/userContext";
import Style from "../style/index.module.css";

const Register = () => {
  let history = useHistory();
  const { request} = useUser();
  const [error, setError] = useState(null);
  const [loading, setLoading] = useState(null);
  const emailRef = useRef();
  const passwordRef = useRef();
  const usernameRef = useRef();
  const bioRef = useRef();
  const cityRef = useRef();
  const socialRef = useRef();
  useEffect(() => {
    if(loading) {
      setLoading((oldLoading) => { return false});
    } return () => {
      setLoading((oldLoading) => { return null});
    }
  },[error])
  function handleSubmit (e) {
    e.preventDefault();
    setError((oldErr) => {return null})
    const account ={
      email : emailRef.current.value,
      password: passwordRef.current.value,
      username: usernameRef.current.value,
      bio: bioRef.current.value,
      city: cityRef.current.value,
      social_media_url: socialRef.current.value,
    }
    try {
      setLoading((oldLoading) => { return true})
      request('/api/register','POST', account).then((data) => {
        if(data.code > 0) {
          setError( (oldError) => {return data.message });
        } else {
          history.push('/login')
        }
      }).catch((err) => {
        setError((oldErr) => {return err.message})
      })
    } catch(err) {
      setError( (oldError) => { return err.message })
    }
  }
  return (
    <div className={Style.card}>
            <h2>Register</h2>
            { error && <div className="alert alert-danger">{error}</div>}
           <form onSubmit={handleSubmit}>
            <div className={Style.inputBorder}>
                <input ref={usernameRef} type="text" id="name" className={Style.text} name="name" required/>
                <label htmlFor="name">Name</label>
                <div className={Style.border}></div>
            </div>
            
            <div className={Style.inputBorder}>
             <input ref={emailRef} type="email" id="email" className={Style.text} name="email" required/>
             <label htmlFor="email">Email</label>
             <div className={Style.border}></div>
            </div>
            
            <div className={Style.inputBorder}>
             <input ref={passwordRef} type="password" id="pasword" className={Style.text} name="password" required/>
             <label htmlFor="password">Password</label>
             <div className={Style.border}></div>
            </div>
            
            <div className={Style.inputBorder}>
             <input ref={bioRef} type="text" id="bio" className={Style.text} name="bio"/>
             <label htmlFor="bio">Bio</label>
             <div className={Style.border}></div>
            </div>

            <div className={Style.inputBorder}>
             <input ref={cityRef} type="text" id="city" className={Style.text} name="city"/>
             <label htmlFor="city">City</label>
             <div className={Style.border}></div>
            </div>

            <div className={Style.inputBorder}>
             <input ref={socialRef} type="text" id="social_media_url" className={Style.text} name="social_media_url"/>
             <label htmlFor="social_media_url">Social Media</label>
             <div className={Style.border}></div>
            </div>

            <button disabled={loading} type="submit" className={Style.btn} value="Log In">Register</button>
           </form>
            <div className="d-flex">
                <Link to="/login" className="opacity-75 text-decoration-none pt-3 mx-1 text-white">Login</Link>
            </div>
           </div>
  );
};

export default Register;
