import React, { useEffect, useRef, useState } from "react";
import Style from "../style/index.module.css";
import { Link, useHistory } from "react-router-dom";
import { useUser } from "../component/userContext";

const Login = () => {
  let history = useHistory();
  const {activeUser, setActiveUser, request, setGlobalError, globalError} = useUser();
  const [error, setError] = useState(null);
  const [loading, setLoading] = useState(false);
  const emailRef = useRef();
  const passwordRef = useRef();

  useEffect(() => {
    if(loading) {
      setLoading((oldLoading) => { return false});
    } return () => {
      setLoading((oldLoading) => { return null});
    }
  },[error])

  useEffect(() => {
    if(loading) {
      history.push('/home')
    } return () => {
      setLoading((oldLoading) => { return null});
    }
  },[activeUser])

  function handleSubmit (e) {
    e.preventDefault();
    setGlobalError((oldError) => {return null})
    setError((oldErr) => {return null})
    const account ={
      email : emailRef.current.value,
      password: passwordRef.current.value,
    }
    try {
      setLoading((oldLoading) => {return true});
      request('/api/login','POST', account).then( (data) => {
        if(data.code > 0) {
          setError( (oldError) => {return data.message });
        } else {
          setActiveUser((old) => {return null})
          localStorage.setItem('token', data.data.token);
          setActiveUser((old) => {return account.email});
        }
      }).catch((err) =>{
        setError((oldErr) => {return err.message})
      })
    } catch(err) {
      setError( (oldError) => { return err.message })
    }
  }

  return (
    <div className={Style.card}>
      <h2>Log In</h2>
      <form onSubmit={handleSubmit}>
        { error && <div className="alert alert-danger">{error}</div>}
        { globalError && <div className="alert alert-danger">{globalError}</div>}

        <div className={Style.inputBorder}>
          <input ref={emailRef} type="text" className={Style.text} name="email" required/>
          <label>Email</label>
          <div className={Style.border}></div>
        </div>
        
        <div className={Style.inputBorder}>
          <input ref={passwordRef} type="password" className={Style.text} name="password" required/>
          <label>Password</label>
          <div className={Style.border}></div>
        </div>
        
        <button disabled={loading} type="submit" className={Style.btn} value="Log In">Log in</button>
      </form>
      <div className="d-flex">
      <Link to="/register" className="opacity-75 text-decoration-none pt-4 mx-1 text-white">Register</Link>
      <Link to="/forgot-password" className="opacity-75 text-decoration-none pt-4 mx-3 text-white">Forgot Password</Link>
      </div>
    </div>
  );
};

export default Login;
