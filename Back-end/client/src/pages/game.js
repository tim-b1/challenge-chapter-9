import React, { useEffect, useRef, useState } from 'react';
import { useHistory } from 'react-router';
import { useUser } from '../component/userContext';
import guntingImg from '../style/game-image/gunting.png'
import batuImg from '../style/game-image/batu.png'
import kertasImg from '../style/game-image/kertas.png'
import refresh from '../style/game-image/refresh.png'
import Style from '../style/game.module.css'
import {Player, RockPaperScissor} from '../gameClass';

const Game = (props) => {
    const {request, setGlobalError, globalError} = useUser();
    const [loading, setLoading] = useState(false);
    const totalScoreRef = useRef();
    const [error, setError] = useState(null);
    const param = props.match.params.name
    let history = useHistory();

    let player1 = new Player("player");
    let player2 = new Player("computer");
    let newGame = new RockPaperScissor(player1, player2);

    //Reference Enemy and Player Hand Dom
    const batu = useRef();
    const kertas = useRef();
    const gunting = useRef();
    const batuPlayer = useRef();
    const kertasPlayer = useRef();
    const guntingPlayer = useRef();
    const comHand =[batu, gunting, kertas]
    const playerHand =[batuPlayer, guntingPlayer, kertasPlayer]

    //Reference result 
    const vs = useRef();
    const pWin = useRef();
    const comWin = useRef();
    const draw = useRef();
    const gameResult = [pWin, comWin, draw]
    
    //Increasing game num play
    function handleIncrease(e) {
        for(let i=0; i< comHand.length; i++) {
            comHand[i].current.style.backgroundColor = "transparent";
            playerHand[i].current.style.backgroundColor="transparent";
            gameResult[i].current.style.display ="none";
        }
        vs.current.style.display= "block"
        if(player1.numWin > 0) {
            e.preventDefault();
            setError((oldErr) => { return null})
            setGlobalError((oldErr) => { return null})
            setLoading((oldLoading) => {return true})
            try{
                request(`/api/game/increasePlayCount/${param}`, 'PUT', {number : parseInt(newGame.playCount)}).then((data) => {
                    if(data.code > 0) {
                        if(data.code === 150) {
                            setGlobalError((oldError) => {return data.message})
                        } else {
                            setError((oldError) => {return data.message})
                        }
                    } else {
                        newGame.playCount = 0;

                        //Increasing player score
                        try {
                            setLoading((oldLoading) => { return true})
                            request('/api/profile/edit','PUT', {total_score : parseInt(player1.numWin) }).then( (data) => {
                                if(data.code > 0) {
                                    if(data.code === 150) {
                                        setGlobalError((oldError) => {return data.message})
                                    }
                                    setError( (oldError) => {return data.message });
                                } else {
                                    totalScoreRef.current.innerHTML = 0;
                                    player1.numWin = 0;
                                    setLoading((oldLoading) => {return false})
                                }
                                }).catch((err) => {
                                    setError((oldError) => {return err.message})
                                })
                        } catch(err) {
                            setError( (oldError) => { return err.message })
                        }
                        
                    }
                }).catch((err) => {
                    setError((oldErr) => {return err.message})
                })
            } catch(err){
                setError((oldErr) => {return err.message})
            }
        }
        setLoading((oldLoading) => {return false})
    }

    function handleClick(e) {
        setLoading((oldLoading) => {return true})
        e.stopPropagation();
        e.preventDefault();
        for(let i=0; i< comHand.length; i++) {
            comHand[i].current.style.backgroundColor = "transparent";
            playerHand[i].current.style.backgroundColor="transparent";
            gameResult[i].current.style.display ="none";
        }
        vs.current.style.display= "none"
        e.target.style.backgroundColor = "#C4C4C4";
        player1.choice = e.target.value;
        newGame.playGame();
        comHand[player2.random].current.style.backgroundColor = "#C4C4C4";
        gameResult[newGame.result].current.style.display ="block";
        totalScoreRef.current.innerHTML = player1.numWin;
        setLoading((oldLoading) => {return false})
    }

    useEffect( () => {
        if(!!globalError) {
            history.push('/login')
        } return () => {
            setLoading((oldLoading) => {return false});
        }
    },[globalError])
    
    return (
       <div className="p-0">
        <div style={{backgroundColor: "#998263", borderRadius:"6px"}} className = {"container-fluid pt-3 pb-3 " + Style.theGame}>
        {error && <h5 className="text-warning text-center font-weight-bold">{error}</h5>}

            <div className ="row p-lg-0 m-lg-0 p-3 m-3">

                <div className="col-5 d-flex justify-content-end">
                        <div className ={"d-flex justify-content-around flex-column " + Style.theImage}>
                            
                            <div className="text-center">
                                <div className={Style.imageTitle}> PLAYER <span style={{color:"transparent"}}>.</span>
                                    <span ref={totalScoreRef}>0</span> </div>
                            </div>

                            <button className="w-75 d-flex justify-content-center" ref={batuPlayer} value="batu" onClick={handleClick} disabled={loading} style={{border:"none",borderRadius:"1vw", backgroundColor:"transparent"}}>
                                <div style={{pointerEvents:"none"}} className="text-center m-3 p-1">
                                    <img alt="batu" style={{pointerEvents:"none"}} className={Style.batu} src={batuImg}/>
                                </div>
                            </button>

                            <button className="w-75 d-flex justify-content-center" ref={guntingPlayer} value="gunting" onClick={handleClick} disabled={loading} style={{border:"none",borderRadius:"1vw", backgroundColor:"transparent"}}>
                                <div style={{pointerEvents:"none"}} className="text-center m-3 p-1">
                                    <img alt="gunting" style={{pointerEvents:"none"}} className={Style.tangan} src={guntingImg}/>                                
                                </div>
                            </button>

                            <button className="w-75 d-flex justify-content-center" ref={kertasPlayer} value="kertas" onClick={handleClick} disabled={loading} style={{border:"none",borderRadius:"1vw", backgroundColor:"transparent"}}>
                                <div style={{pointerEvents:"none"}} className="text-center m-3 p-1">
                                    <img alt="kertas" style={{pointerEvents:"none"}} className={Style.tangan} src={kertasImg}/>
                                </div>
                            </button>
                        </div>
                </div>

                <div className={"col-2 d-flex align-items-end justify-content-center p-0" + Style.tabTengah}>
                    
                    <div ref={vs} className={" m-3 p-3 text-center " + Style.vs}> VS </div>
                    <div ref={pWin} className={Style.playerWin+ " m-3 p-3 text-center" }> <p className={Style.playerWinP}> PLAYER<span style={{color: "transparent"}}>.</span>1 WIN </p> </div>
                    <div ref={comWin} className={Style.computerWin+" m-3 p-3 text-center"}> <p className={Style.computerWinP}> COM WIN </p> </div>
                    <div ref={draw} className={Style.draw + " m-3 p-3 text-center"}> <p className={Style.drawP}> DRAW </p> </div>
                </div>

                <div className="col-4 d-flex justify-content-start">
                    <div className ={"d-flex justify-content-around flex-column p-0 " + Style.theImage}>
                        
                        <div className="pr-3 mr-3 text-right">
                            <div className={Style.imageTitle}> COM </div>
                        </div>

                        <div className="align-self-end w-75  d-flex justify-content-center" ref={batu} style={{borderRadius:"1vw"}}>
                            <div className="text-center my-3 p-1">
                                <img alt="batu" className={Style.batu2}  src={batuImg}/>
                            </div>
                        </div>

                        <div className=" align-self-end w-75  d-flex justify-content-center"  ref={gunting} style={{borderRadius:"1vw"}}>
                            <div className="text-center m-3 p-1">
                                <img alt="gunting" className={Style.tangan2} src={guntingImg}/>
                            </div>
                        </div>

                        <div className="align-self-end w-75  d-flex justify-content-center" ref={kertas} style={{borderRadius:"1vw"}}>
                            <div className="text-center m-3 p-1">
                                <img alt="kertas" className={Style.tangan2}src={kertasImg} />
                            </div>
                        </div>
                    </div>
                </div>
                <button onClick={handleIncrease} disabled={loading} style={{border:"none",borderRadius:"1vw", backgroundColor:"transparent"}} className="text-center">
                    <img className={Style.resetButton} src={refresh}/>
                </button>

            </div>    
        </div>
       </div>
    )
}


export default Game