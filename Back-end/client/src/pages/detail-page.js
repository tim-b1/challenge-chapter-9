import 'bootstrap/dist/css/bootstrap.min.css';
import GameDetail from '../component/game-detail';
import LeaderBoard from '../component/leaderboard';

function DetailPage(props) {
    const param = props.match.params.name
    return (
        <>
            <GameDetail param = {param}/>
            <LeaderBoard />
        </>
    );
}

export default DetailPage;
