import React, { useEffect, useRef, useState } from "react";
import { Link, useHistory } from "react-router-dom";
import { useUser } from "../component/userContext";
import 'bootstrap/dist/css/bootstrap.min.css';

const GameList = () => {
    const {request, setGlobalError, globalError} = useUser();
    const [loading, setLoading] = useState(false);
    const [linkArray, setLinkArray] = useState(null);
    const [error, setError] = useState(null);
    const linkArrayRef = useRef()
    let history = useHistory()
    useEffect (() => {
        try {
            setError((oldErr) => { return null})
            setGlobalError((oldErr) => { return null})
            setLoading((oldLoading) => {return true})
            request('/api/game/thumbnails', 'GET').then((data) => {
                if(data.code > 0) {
                    if(data.code === 150) {
                        setGlobalError((oldError) => {return data.message})
                    } else {
                        setError((oldError) => {return data.message})
                    }
                } else {
                    setLinkArray((oldAccount) => {return data.data});
                }
            }).catch((err) =>{
                setError((oldErr) => {return err.message})
              })
        } catch(err) {
            setError((oldErr) => {return err.message})
        }
        return () => {
            setLinkArray((oldAccount) => {return null})
        }
    },[])

    useEffect( () => {
        if(linkArray) {
            setLoading((oldLoading) => {return false});
        }
        if(!!globalError) {
            history.push('/login')
        } return () => {
            setLoading((oldLoading) => {return false});
        }
    },[globalError, linkArray])

    useEffect( () => {
        if(!!linkArray) {
            linkArrayRef.current = linkArray.map((item, i) => {
                return (
                    <div key={item.name} className="card bg-light" style= {{width: "18rem"}}>
                        <Link to={{pathname: `/game-list/${item.name}`}}><img key={item.name + Math.random()} className="card-img-top p-3" src={item.thumbnail_url} alt={item.name} /></Link>
                        <div key={item.name + Math.random()} className="card-body">
                            <p key={item.name + Math.random()} className="card-text text-center">{item.name}</p>
                        </div>
                    </div>
                )
            })
        }
    }, [linkArray, loading])

    return (
        <>
        { error && <div className=" alert alert-danger">{error}</div>}
        <div className="p-0">{!loading && linkArrayRef.current && linkArrayRef.current}</div>
        </>
    )
}

export default GameList;
