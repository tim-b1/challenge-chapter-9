export class Player{
    constructor(name){
        this.name = name;
        this.numWin = 0;
        this.choice = "";
        this.random = 0;
    }

    makeChoice() {
        const SELECTIONSLIST = ["batu","gunting","kertas"];
        
        if(this.name == "computer"){ 
            const randomNumber = Math.floor(Math.random() * SELECTIONSLIST.length);
            this.choice = SELECTIONSLIST[randomNumber];
            this.random = randomNumber;
            return this.choice;
        }
        else { 
            return this.choice;
        }
    }
}

export class RockPaperScissor{
    constructor (player1, player2)
    {
        this.recentStatus = undefined;
        this.player1 = player1;
        this.player2 = player2;
        this.result = null;
        this.playCount = 0;
    }

    playGame() {
        let player1Choice = this.player1.makeChoice();
        let player2Choice = this.player2.makeChoice();
        this.playCount = this.playCount + 1;

        const beatList = {
            'gunting' : 'kertas',
            'batu' :'gunting',
            'kertas' : 'batu'
        }

        if(beatList[player1Choice] === player2Choice){
            this.recentStatus = this.player1.name + "Win";
            this.result = 0
            this.player1.numWin++;
        } else if(beatList[player2Choice] === player1Choice){
            this.recentStatus = this.player2.name + "Win";
            this.result = 1
            this.player2.numWin++;
        } else {
            this.recentStatus = "draw";
            this.result = 2
        }
    }

    resetGame(){
        this.player1.numWin = 0;
        this.player2.numWin = 0;
    }
}

