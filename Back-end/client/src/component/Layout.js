import 'bootstrap/dist/css/bootstrap.min.css';
import Menu from "./menu";

function Layout({children}) {
    return (
        <>
            <Menu />
            <div className="p-2 m-2 d-flex justify-content-center row">
                {children}
            </div>
        </>
    );
}

export default Layout;
