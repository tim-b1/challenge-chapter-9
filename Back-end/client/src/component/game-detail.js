import React, { useEffect, useState } from "react";
import { Link, useHistory } from "react-router-dom";
import { useUser } from "../component/userContext";
import 'bootstrap/dist/css/bootstrap.min.css';

const GameDetail = ({param}) => {
    const {request, setGlobalError, globalError} = useUser();
    const [loading, setLoading] = useState(false);
    const [gameDetail, setGAmeDetail] = useState(null);
    const [error, setError] = useState(null);
    let history = useHistory()
    useEffect (() => {
        try {
            setError((oldErr) => { return null})
            setGlobalError((oldErr) => { return null})
            setLoading((oldLoading) => {return true})
            request(`/api/game/${param}`, 'GET').then((data) => {
                if(data.code > 0) {
                    if(data.code === 150) {
                        setGlobalError((oldError) => {return data.message})
                    } else {
                        setError((oldError) => {return data.message})
                    }
                } else {
                    setGAmeDetail((oldAccount) => {return data.data});
                }
            }).catch((err) =>{
                setError((oldErr) => {return err.message})
              })
        } catch(err) {
            setError((oldErr) => {return err.message})
        }
        return () => {
            setGAmeDetail((old) => {return null})
        }
    },[])

    useEffect( () => {
        if(gameDetail) {
            setLoading((oldLoading) => {return false});
        }
        if(!!globalError) {
            history.push('/login')
        } return () => {
            setLoading((oldLoading) => {return false});
        }
    },[globalError, gameDetail])


    return (
        <div className="p-3 m-2" style={{ backgroundImage: (!loading && gameDetail) ? `url(${gameDetail.game_url})` :"",height:"390px" }}>
            {error && <div className="alert alert-danger">{error}</div>}    
            {!loading && gameDetail &&
                <div className="card d-flex float-left" style={{width: "15rem"}}>
                    <img className="bg-light card-img-top p-3" src={gameDetail.thumbnail_url} alt={gameDetail.name}/>
                    <div className="card-body">
                        <h5 className="card-title">{gameDetail.name}<span type="button" data-toggle="tooltip" title="play count" className="m-1 badge bg-secondary">{gameDetail.play_count}</span></h5>
                        <p className="card-text text-muted">{gameDetail.description}</p>
                        <Link to={{pathname: `/game/${param}`}} type="button" className="btn btn-primary">Play</Link>
                    </div>
                </div>
            }
        </div>
    )
}

export default GameDetail;
