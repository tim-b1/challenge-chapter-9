import React, { useEffect, useRef, useState } from "react"
import { useHistory } from "react-router";
import { useUser } from "../component/userContext"

const LeaderBoard = () => {
    const {request, activeAccount, globalError, setGlobalError} = useUser();
    const [allAcount, setAllAccount] = useState();
    const [loading, setLoading] = useState(true);
    const allAccountRef = useRef();
    const [error, setError] = useState("");
    let history = useHistory()

    useEffect (() => {
        try {
            setError((oldError) => {return null})
            setGlobalError((oldError) => {return null})
            setLoading((oldLoading) => {return true});
            request('/api/profile/getall','GET').then((data) => {
                if(data.code > 0) {
                    if(data.code === 150) {
                        setGlobalError((oldError) => {return data.message})
                    } else {
                        setError((oldError) => {return data.message})
                    }
                } else {
                    setAllAccount((oldAccount) => {return data.data});
                }
            }).catch((err) => {
                setError((oldError) => {return err.message})
            })
        } catch (err) {
            setError((oldError) => {return err.message})
        }
        return () => {
            setAllAccount((oldAccount) => {return null})
        }
    },[])

    useEffect( () => {
        if(allAcount) {
            setLoading((oldLoading) => {return false});
        }
        if(!!globalError) {
            history.push('/login')
        } return () => {
            setLoading((oldLoading) => {return false});
        }
    },[globalError, allAcount])
    
    useEffect( () => {
        if(!!allAcount) {
            allAccountRef.current = allAcount.map((item, i) => {
                return (
                    <div key={i} className={"card m-3 py-2" + (item.email === activeAccount.email ? ' border-success text-success' : '')} style={{width: "18rem"}}>
                        <div key={item.email} className="card-header">
                            #{i+1}
                        </div>
                        <ul key={item.username + Math.random()} className="list-group list-group-flush">
                            {item.username && <li key={item.username + Math.random()} className="list-group-item">{item.username}</li>}
                            {item.total_score && <li key={item.username + Math.random()} className="list-group-item" data-toggle="tooltip" title="total score">{item.total_score}</li>}
                            {item.city && <li key={item.username + Math.random()} className="list-group-item">{item.city}</li>}
                            {item.bio && <li key={item.username + Math.random()} className="list-group-item">{item.bio}</li>}
                            {item.social_media_url &&<li key={item.username + Math.random()} className="list-group-item">{item.social_media_url}</li>}
                        </ul>
                    </div>
                )
            })
        }
    }, [allAcount, loading])
    
    // let account = Object.keys(activeAccount).map(function(key) {
    //     <li key={key} value={key}>{activeAccount[key]}</li>
    // });

    return (
        <div className="p-0">
            { error && <div className="alert alert-danger">{error}</div>}
            <div className="container d-flex row justify-content-center">
                {!loading && allAccountRef.current && allAccountRef.current}
            </div>
        </div>
    )
}

export default LeaderBoard
