import React, { createContext, useContext, useEffect, useState } from 'react';

export const UserContext = createContext(null);

export function useUser() {
    return useContext(UserContext)
}

function isJSON(str) {
    try {
        JSON.parse(str);
    } catch (e) {
        return false;
    }
    return true;
}

const request = async (endpoint, method, data) => {
    let req;
    let h = new Headers();
    const token = localStorage.getItem('token') 
    if(token){
        h.append('Authorization', `Bearer ${token}`);
    }
    if(method === 'POST' || method === 'PUT') {
        h.append('Content-Type', 'application/json')
        req = new Request(endpoint, {
            method:method,
            headers : h,
            body: JSON.stringify(data)
        })
    } else {
        req = new Request(endpoint, {
            method:method,
            headers:h,
        })
    }
    const content = await (await fetch(req)).json();
    return content;
}
export function UserProvider ({children}) {
    const [activeUser, setActiveUser] = useState(() => {
        const data = localStorage.getItem('user');
        return data ? data : null
    });
    
    const [activeAccount, setActiveAccount] = useState(() => {
        const data = localStorage.getItem('account');
        return isJSON(data) ? JSON.parse(data) : null;
    });
    const [loading, setLoading] = useState(true);
    const [globalError, setGlobalError] = useState(null);
    
    useEffect(() => {
        if(typeof activeAccount == 'object') {
            setLoading( (oldLoading) => { return false })
            localStorage.setItem("account", JSON.stringify(activeAccount))
        } return () => {
            console.log(Math.random())
        }
    },[activeAccount])
    
    useEffect(() => {
        setLoading( (oldLoading) => { return false })
        localStorage.setItem('user', activeUser)
    },[activeUser])
    
    return (
        <UserContext.Provider value={{activeUser, request, setActiveUser, activeAccount, setActiveAccount,globalError, setGlobalError}}>
            {!loading && children}
        </UserContext.Provider>
    )
}

