import React, { useState } from 'react';
import { Link } from 'react-router-dom';
import 'bootstrap/dist/css/bootstrap.min.css';
import {
  Collapse,
  Navbar,
  NavbarToggler,
  Nav,
  NavItem,
} from 'reactstrap';

const Menu = (props) => {
  const [isOpen, setIsOpen] = useState(false);

  const toggle = () => setIsOpen(!isOpen);

  return (
    <div className="container-fluid w-100">
      <Navbar style={{borderRadius : "6px", backgroundColor:"#2F4E6F"}} className="m-1 p-md-0 p-2 navbar-dark" expand="md">
      <Link to="/home" className="text-decoration-none text-white navbar-brand px-3 mx-3">Home</Link>
        <NavbarToggler onClick={toggle} />
        <Collapse isOpen={isOpen} navbar>
          <Nav className="p-md-1 pt-3" navbar>
            <NavItem className="p-md-2 m-md-2 pt-2 m-3 text-center">
              <Link to="/profile" href="/components/" className="opacity-75 text-white text-decoration-none">Profile</Link>
            </NavItem>
            <NavItem className="px-md-3 p-md-2 mx-md-3 m-md-2 pb-2 text-center">
              <Link to="/game-list" className="opacity-75 text-white text-decoration-none">Game List</Link>
            </NavItem>
          </Nav>
        </Collapse>
      </Navbar>
      </div>
  );
}

export default Menu;