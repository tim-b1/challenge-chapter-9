import React from 'react';
import { Route, Redirect } from 'react-router';
import { useUser } from './userContext';

function ProtectedRoute({component : Component, ...rest}) {
    const {activeUser} = useUser(); 
    return (
        <Route {...rest} render = {props => {
            return (activeUser !="null" || !!activeUser) ? <Component {...props} /> : <Redirect to="/login" />
        }}>
        </Route>
    )
}

export default ProtectedRoute;