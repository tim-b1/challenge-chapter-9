import React from "react";
import Style from "../style/game.module.css";

const Game = () => {
  return (
    <div id={Style.container} style={{backgroundColor: "#9C835F"}}>
      <div className={Style.gamename}>
        <a className={Style.back} href="/" style={{ fontSize: "40px", textDecoration: "none", color: "black" }}><b>&lt;</b></a>
        <img src="Images/logo game.png" alt="logo game" style={{width:"50px", height:"50px"}}></img>
        <p style={{ fontSize: "30px", color: "#F9B23D" }}>
          <b>ROCK PAPPER SCISSORS</b>
        </p>
      </div>

      <div className={Style.gamecontainer}>
        <div id={Style.player}>
          <p className={Style.player}>PLAYER 1</p>
          <ul>
            <li>
              <img
                className={Style.game}
                src="Images/batu.png"
                alt="batu"
                style={{ width: "80px" }}
              />
            </li>
            <li>
              <img
                className={Style.game}
                src="Images/kertas.png"
                alt="kertas"
                style={{ width: "80px" }}
              />
            </li>
            <li>
              <img
                className={Style.game}
                src="Images/gunting.png"
                alt="gunting"
                style={{ width: "80px" }}
              />
            </li>
          </ul>
        </div>

        <p className={Style.vs} style={{ position: "absolute", top: "170px" }}>
          VS
        </p>

        <div id={Style.com}>
          <p className={Style.player}>COM</p>
          <ul>
            <li>
              <img
                className={Style.game}
                src="Images/batu.png"
                alt="batu"
                style={{ width: "80px" }}
              />
            </li>
            <li>
              <img
                className={Style.game}
                src="Images/kertas.png"
                alt="kertas"
                style={{ width: "80px" }}
              />
            </li>
            <li>
              <img
                className={Style.game}
                src="Images/gunting.png"
                alt="gunting"
                style={{ width: "80px" }}
              />
            </li>
          </ul>
        </div>
      </div>

      <img src="Images/refresh.png" id={Style.refresh} style={{ width: "40px" }} />
    </div>
  );
};

export default Game;
