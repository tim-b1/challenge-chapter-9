import React from "react";
import Style from "../style/index.module.css";

const Login = () => {
  return (
    <div className={Style.bungkus}>
    <div className={Style.card}>
            <h2>Log-In to Dashboard</h2>
           
            <form action="/login" method="post">
            <div className={Style.inputBorder}>
             <input type="text" className={Style.text} name="email" required/>
             <label>Email</label>
             <div className={Style.border}></div>
            </div>
            
            <div className={Style.inputBorder}>
             <input type="password" className={Style.text} name="password" required/>
             <label>Password</label>
             <div className={Style.border}></div>
            </div>
            
            <button href="/artikel" type="submit" className={Style.btn} value="Log In">Log in</button>
           </form>
           <a href="/register"><br/>Register</a>
    </div>
    </div>
  );
};

export default Login;
