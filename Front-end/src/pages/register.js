import React from "react";
import Style from "../style/index.module.css";

const Register = () => {
  return (
    <div className={Style.bungkus}>
    <div className={Style.card}>
            <h2>Halaman Register</h2>
           
           <form action="/register" method="post">
            <div className={Style.inputBorder}>
                <input type="text" id={Style.name} className={Style.text} name="name" required/>
                <label for="name">Name</label>
                <div className={Style.border}></div>
               </div>
            
            <div className={Style.inputBorder}>
             <input type="email" id={Style.email} className={Style.text} name="email" required/>
             <label for="email">Email</label>
             <div className={Style.border}></div>
            </div>
            
            <div className={Style.inputBorder}>
             <input type="password" id={Style.pasword} className={Style.text} name="password" required/>
             <label for="password">Password</label>
             <div className={Style.border}></div>
            </div>
            
            <button href="/login" type="submit" className={Style.btn} value="Log In">Register</button>
           </form>
           </div>
           </div>
  );
};

export default Register;
