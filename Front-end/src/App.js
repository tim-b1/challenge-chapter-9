import React from "react";
import "./App.css";
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom';

import Home from './pages/home'
import User from './pages/user'
import Game from './pages/game'
import Login from './pages/login'
import Register from './pages/register'

function App() {
  return (
    <div>
      <Router>
        <Switch>
          <Route exact path="/" component={Home}/>
          <Route exact path="/user" component={User}/>
          <Route exact path="/game" component={Game}/>
          <Route exact path="/login" component={Login}/>
          <Route exact path="/register" component={Register}/>
        </Switch>
      </Router>
    </div>
  );
}

export default App;
